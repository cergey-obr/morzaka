//
//  AppDelegate.h
//  Morzaka
//
//  Created by Alexander Vlaznev on 15.05.14.
//  Copyright (c) 2014 vlaznevbro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
