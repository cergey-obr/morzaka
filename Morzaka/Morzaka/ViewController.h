//
//  ViewController.h
//  Morzaka
//
//  Created by Alexander Vlaznev on 15.05.14.
//  Copyright (c) 2014 vlaznevbro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextViewDelegate> {
    NSDictionary *morse;
    NSInteger limit;
}

- (IBAction)touch:(id)sender;
- (IBAction)share:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *textField;
@property (weak, nonatomic) IBOutlet UILabel *counterLabel;
@property (weak, nonatomic) IBOutlet UIButton *translateButton;

@end
