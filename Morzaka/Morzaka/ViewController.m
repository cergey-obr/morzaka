//
//  ViewController.m
//  Morzaka
//
//  Created by Alexander Vlaznev on 15.05.14.
//  Copyright (c) 2014 vlaznevbro. All rights reserved.
//

#import "ViewController.h"
#import <AudioToolbox/AudioToolbox.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    limit = 140;
    NSString *lang = [[NSLocale preferredLanguages] objectAtIndex: 0];
    if ([lang isEqual: @"ru"]) {
        morse = @{
                     @"А": @".-",
                     @"Б": @"-...",
                     @"В": @".--",
                     @"Г": @"--.",
                     @"Д": @"-..",
                     @"Е": @".",
                     @"Ж": @"...-",
                     @"З": @"--..",
                     @"И": @"..",
                     @"Й": @".---",
                     @"К": @"-.-",
                     @"Л": @".-..",
                     @"М": @"--",
                     @"Н": @"-.",
                     @"О": @"---",
                     @"П": @".--.",
                     @"Р": @".-.",
                     @"С": @"...",
                     @"Т": @"-",
                     @"У": @"..-",
                     @"Ф": @"..-.",
                     @"Х": @"....",
                     @"Ц": @"-.-.",
                     @"Ч": @"---.",
                     @"Ш": @"----",
                     @"Щ": @"--.-",
                     @"Ъ": @".--.-.",
                     @"Ы": @"-.--",
                     @"Ь": @"-..-",
                     @"Э": @"..-..",
                     @"Ю": @"..--",
                     @"Я": @".-.-",
                     @"1": @".----",
                     @"2": @"..---",
                     @"3": @"...--",
                     @"4": @"....-",
                     @"5": @".....",
                     @"6": @"-....",
                     @"7": @"--...",
                     @"8": @"---..",
                     @"9": @"----.",
                     @"0": @"-----",
                     @".": @"......",
                     @",": @".-.-.-",
                     @":": @"---...",
                     @";": @"-.-.-.",
                     @"(": @"-.--.-",
                     @")": @"-.--.-.",
                     @"'": @".----.",
                     @"-": @"-....-",
                     @"/": @"-..-.",
                     @"?": @"..--..",
                     @"!": @"--..--",
                     @"@": @".--.-."
        };
    } else {
        morse = @{
                     @"A": @".-",
                     @"B": @"-...",
                     @"W": @".--",
                     @"G": @"--.",
                     @"D": @"-..",
                     @"E": @".",
                     @"V": @"...-",
                     @"Z": @"--..",
                     @"I": @"..",
                     @"J": @".---",
                     @"K": @"-.-",
                     @"L": @".-..",
                     @"M": @"--",
                     @"N": @"-.",
                     @"O": @"---",
                     @"P": @".--.",
                     @"R": @".-.",
                     @"S": @"...",
                     @"T": @"-",
                     @"U": @"..-",
                     @"F": @"..-.",
                     @"H": @"....",
                     @"C": @"-.-.",
                     @"Q": @"--.-",
                     @"Y": @"-.--",
                     @"X": @"-..-",
                     @"1": @".----",
                     @"2": @"..---",
                     @"3": @"...--",
                     @"4": @"....-",
                     @"5": @".....",
                     @"6": @"-....",
                     @"7": @"--...",
                     @"8": @"---..",
                     @"9": @"----.",
                     @"0": @"-----",
                     @".": @"......",
                     @",": @".-.-.-",
                     @":": @"---...",
                     @";": @"-.-.-.",
                     @"(": @"-.--.-",
                     @")": @"-.--.-.",
                     @"'": @".----.",
                     @"-": @"-....-",
                     @"/": @"-..-.",
                     @"?": @"..--..",
                     @"!": @"--..--",
                     @"@": @".--.-."
        };
    }
    [self.textField becomeFirstResponder];
    self.counterLabel.text = [NSString stringWithFormat: @"%d", limit];
}

/*
 Обработка символов в строке
 */
- (IBAction)touch:(id)sender {
	NSString *text = self.textField.text;
    self.textField.text = [self translator: text];
	
	UIImage *buttonText = [UIImage imageNamed:@"button-text"];
	UIImage *buttonMorse = [UIImage imageNamed:@"button-morse"];
	
	BOOL morseWord = [self detectMorseStr: text];
	if (morseWord) [self.translateButton setImage: buttonText forState: UIControlStateNormal];
	else [self.translateButton setImage: buttonMorse forState: UIControlStateNormal];
}



/*
 Обработка ввода символов с клавиатуры
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSInteger currentTextLength = [textView.text length] - range.length + [text length];
    // скрываем клавиатуру, если нажали на "готово"
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return false;
    }
    // вычисляем размер текста
    if (currentTextLength > 8) {
        CGFloat size = (limit / currentTextLength) + 17;
        [textView setFont: [UIFont systemFontOfSize: size]];
    } else {
        [textView setFont: [UIFont systemFontOfSize: 45]];
    }
    // включаем вибрацию и shake-анимацию, если лимит символов превышен
    if (currentTextLength > limit) {
        [self shake: 10 direction: 1 currentTimes: 0 withDelta: 5 andSpeed: 0.03];
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        return false;
    }
    // высчитываем остатки
    NSInteger remains = limit - currentTextLength;
    self.counterLabel.text = [NSString stringWithFormat: @"%d", remains];
    return true;
}



/*
 Переводчик
 */
- (NSString *)translator: (NSString *)text {
    BOOL morseWord = [self detectMorseStr: text];
    return morseWord ? [self morseToWords: text] : [self wordsToMorse: text];
}



/*
 Перевод слова в морзянку
 */
- (NSString *)wordsToMorse: (NSString *)encodeStr {
    NSMutableString *str = [NSMutableString string];
    NSString *text = [encodeStr uppercaseString];
    NSUInteger length = [text length];
    for (int i = 0; i < length; i++) {
        NSString *symbol = [text substringWithRange:NSMakeRange(i, 1)];
        NSString *morseSymbol = [morse valueForKey: symbol];
        if (morseSymbol == nil) morseSymbol = @" ";
        NSString *format = i == (length - 1) ? @"%@" : @"%@ ";
        [str appendFormat: format, morseSymbol];
    }
    return str;
}



/*
 Перевод морзянки в буквы
 */
- (NSString *)morseToWords: (NSString *)decodeStr {
    NSMutableString *str = [NSMutableString string];
    NSArray *symbolsArray = [decodeStr componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    NSUInteger countArray = [symbolsArray count];
    for (int i = 0; i < countArray; i++) {
        NSString *normalSymbol = [self searchNormalSymbol: [symbolsArray objectAtIndex: i]];
        if (normalSymbol == nil) normalSymbol = @" ";
        [str appendFormat:@"%@", normalSymbol];
    }
    return [[self stripDoubleSpaceFrom: str] lowercaseString];
}



/*
 Поиск буквы в словаре морзянки
 */
- (NSString *)searchNormalSymbol: (NSString *)morseSymbol {
    NSArray *keys = [morse allKeys];
    for (NSString *keyItem in keys) {
        if ([[morse objectForKey: keyItem] isEqual: morseSymbol]) return keyItem;
    }
    return NULL;
}



/*
 Удаление двойных пробелов из строки
 */
- (NSString *)stripDoubleSpaceFrom:(NSString *)str {
    while ([str rangeOfString:@"  "].location != NSNotFound) {
        str = [str stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    return str;
}



/*
 Определение морзянки в слове (точка, тире, пробел)
 */
- (BOOL) detectMorseStr:(NSString *)string {
    NSError *error = NULL;
    NSString *expression = @"^[-.\\s]*$";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern: expression options: NSRegularExpressionCaseInsensitive error: &error];
    NSTextCheckingResult *match = [regex firstMatchInString: string options: 0 range: NSMakeRange(0, [string length])];
    return match ? true : false;
}




/*
 Кнопка шаринга
 */
- (IBAction)share:(id)sender {
    NSString *shareText = self.textField.text;
    NSArray *items = @[shareText];
    
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems: items applicationActivities: nil];
    
    [self presentViewController: activity animated: YES completion: nil];
}



/*
 Shake-анимация
 */
- (void)shake:(int)times direction:(int)direction currentTimes:(int)current withDelta:(CGFloat)delta andSpeed:(NSTimeInterval)interval {
	[UIView animateWithDuration:interval animations:^{
        self.textField.transform = CGAffineTransformMakeTranslation(delta * direction, 0);
	} completion:^(BOOL finished) {
		if (current >= times) {
			self.textField.transform = CGAffineTransformIdentity;
			return;
		}
		[self shake:(times - 1) direction:direction * -1 currentTimes:current + 1 withDelta:delta andSpeed:interval];
	}];
}

- (IBAction)translateButton:(id)sender {
}
@end

